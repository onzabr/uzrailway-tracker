import axios from 'axios'
import config from './config.js'

const headers = {
  'accept': 'application/json',
  'accept-language': 'ru',
  'cache-control': 'no-cache',
  'content-type': 'application/json',
  'device-type': 'BROWSER',
}

export var http = axios.create({ baseURL: config.api, headers })

export const auth = function() {
  console.log('Авторизация...')
  return new Promise((resolve, reject) => {
    http.post('auth/login', {
      username: config.username,
      password: config.password
    }).then(({ data }) => {
      http.defaults.headers.common['authorization'] = `Bearer ${data.token}`
      console.log('Авторизован успешно')
      resolve()
    }).catch(error => {
      console.log('Ошибка авторизации', error)
      reject()
    })
  })
}

http.interceptors.response.use(
  (response) => response, 
  (error) => {
    if (error.response.status === 401) {
      console.log('Токен устарел')
      auth()
    }
    return Promise.reject(error);
  }
);

export default { http, auth }