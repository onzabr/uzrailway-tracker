import inquirer from 'inquirer'
import _ from 'lodash'
import notifier from 'node-notifier'
import path from 'path'
import { http, auth } from './http.js'
import { timeLog } from './tools.js'
import directions from './directions.js'

var params = { from: null, to: null, date: null }
var trainsBuffer = null

function checkTrains() {
  http.post('trains/availability/space/between/stations', {
    stationFrom: params.from,
    stationTo: params.to,
    detailNumPlaces: 1,
    showWithoutPlaces: 0,
    direction: [{ depDate: params.date, fullday: true, type: 'Forward' }]
  }).then(({ data }) => {
    process.stdout.write("\u001b[2J\u001b[0;0H");
    const trainsAvailable = data.express.direction[0].trains[0].train

    // check for data changes
    if (trainsBuffer && !_.isEqual(trainsAvailable, trainsBuffer)) {
      notifier.notify(
        {
          title: 'Uzrailway Tracker',
          message: 'Изменения в билетах, проверьте коммандную строку.',
          icon: path.join('./icon.jpg'),
          sound: true
        }
      )
    }
    trainsBuffer = trainsAvailable

    // checking for seats
    const freeSeats = trainsAvailable.reduce((seats, train) => train.places.cars.length + seats, 0)
    if (freeSeats === 0) {
      timeLog(`Места не найдены...`)
      return false;
    }

    // detailing
    timeLog(`Места найдены \r\n`)
    trainsAvailable.map(train => {
      // train.brand - тип поезда
      // train.departure.time - время отправления
      // train.arrival.time - время прибытия
      // train.timeInWay - время в пути
      // train.places.cars.length - наличие мест
      // cars.type - тип места
      // cars.freeSeats - кол-во свободных мест по типу

      if (train.places.cars.length > 0) {
        console.log(`[${train.departure.time} - ${train.arrival.time}] ${train.brand} (в пути ${train.timeInWay})`)
        train.places.cars.map(type => {
          console.log(`    ${type.type}, ${type.freeSeats} мест:`)
          type.tariffs.tariff.map(tarrif => {
            let price = parseInt(tarrif.tariff) + parseInt(tarrif.comissionFee)
            let count = 0
            Object.keys(tarrif.seats).map(key => {
              if (tarrif.seats[key]) count += parseInt(tarrif.seats[key])
            })            
            console.log(`    [${count}] ${tarrif.classService.type} - ${price.toLocaleString()} сум`)
          })
        })
        console.log(' ')
      }
    })
  }).finally(() => {
    setTimeout(checkTrains, 5000)
  })
}

process.stdout.write("\u001b[2J\u001b[0;0H");
inquirer
  .prompt([
    { type: 'list', name: 'from', message: 'Откуда:', choices: directions },
    { type: 'list', name: 'to', message: 'Куда:', choices: directions },
    { type: 'input', name: 'date', message: 'Дата отправления (dd.mm.yyyy):' },
  ])
  .then(answers => {
    params = answers
    auth().then(checkTrains)
  })