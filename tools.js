export const timeLog = function(message) {
  console.log(`[${new Date().toLocaleTimeString()}] ${message}`)
}